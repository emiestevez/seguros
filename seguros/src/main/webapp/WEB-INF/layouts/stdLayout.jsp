<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<html>
    <head>
        <title><tiles:getAsString name="title"/></title>

        <script type="text/javascript" src="./bootstrap/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="./bootstrap/css/bootstrap.min.css" type="text/css" />
        <link rel="stylesheet" href="./css/seguros.css" type="text/css" />
    </head>
    <body>
        <table>
            <tr>
                <td colspan="2">
                    <tiles:insertAttribute name="header" />
                </td>
            </tr>
            <tr>
                <td>
                    <tiles:insertAttribute name="body" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <tiles:insertAttribute name="footer" />
                </td>
            </tr>
        </table>
    </body>
</html>