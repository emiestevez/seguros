/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seguros.service;

import com.seguros.data.domain.Cliente;
import com.seguros.data.repository.ContactoRepository;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Emiliano
 */
@Service
public class ContactoService {
    @Autowired
    private ContactoRepository contactoRepository;
    
    public List<Cliente> findAll(){
        return (List<Cliente>) contactoRepository.findAll();
    }
}
