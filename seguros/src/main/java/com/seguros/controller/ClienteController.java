/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seguros.controller;

import com.seguros.data.domain.Cliente;
import com.seguros.service.ContactoService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Emiliano
 */
@Controller
@RequestMapping("/cliente/")
public class ClienteController {

    @Autowired
    private ContactoService contactoService;

    @RequestMapping(method = RequestMethod.GET)
    public List<Cliente> buscar() {
        return contactoService.findAll();
    }
}
