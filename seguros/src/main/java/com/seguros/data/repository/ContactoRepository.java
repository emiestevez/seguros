/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seguros.data.repository;

import com.seguros.data.domain.Cliente;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author Emiliano
 */
public interface ContactoRepository extends CrudRepository<Cliente, Long> {
}
