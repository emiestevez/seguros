drop table if exists cliente;

create table cliente(
    id bigint identity primary key,
    nombre varchar(100) not null,
    email varchar(100) not null
);

insert into cliente (id, nombre, email) values (1, 'Alejandra','alejandra@mail.com');
insert into cliente (id, nombre, email) values (2, 'Santi','santi@mail.com');
