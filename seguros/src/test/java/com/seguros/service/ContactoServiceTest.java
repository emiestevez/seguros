/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seguros.service;

import com.seguros.data.domain.Cliente;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author Emiliano
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context.xml", "classpath:dataSource.xml"})
public class ContactoServiceTest {
    @Autowired
    private ContactoService contactoService;

    @Test
    public void findAll_conClientesEnLaBd_retornaTodosLosClientes() {
        List<Cliente> clientes = (List<Cliente>) contactoService.findAll();

        Assert.assertEquals(2, clientes.size());
    }
}
