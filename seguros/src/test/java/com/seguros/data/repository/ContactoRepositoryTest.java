/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seguros.data.repository;

import com.seguros.data.domain.Cliente;
import java.util.List;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Test de {@link ContactoRepository}
 *
 * @author Emiliano
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context.xml", "classpath:dataSource.xml"})
public class ContactoRepositoryTest {

    @Autowired
    private ContactoRepository contactoRepository;

    @Test
    public void findAll_conClientesEnLaBd_retornaTodosLosClientes() {
        List<Cliente> clientes = (List<Cliente>) contactoRepository.findAll();

        Assert.assertEquals(2, clientes.size());
    }
}
