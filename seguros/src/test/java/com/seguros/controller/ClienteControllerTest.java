/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.seguros.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.server.setup.MockMvcBuilders;
import static org.springframework.test.web.server.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.server.result.MockMvcResultMatchers.status;

/**
 *
 * @author Emiliano
 */
@RunWith(value = SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:context.xml", "classpath:dataSource.xml"})
public class ClienteControllerTest {
    @Autowired
    private ClienteController clienteController;

    @Test
    public void findAll_conClientesEnLaBd_retornaTodosLosClientesYCodigo200() throws Exception {
        MockMvcBuilders.standaloneSetup(clienteController).build().
                perform(get("/cliente/")).
                andExpect(status().isOk());

    }
}
